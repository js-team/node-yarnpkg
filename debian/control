Source: node-yarnpkg
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Paolo Greppi <paolo.greppi@libpf.com>, Israel Galadima <izzygaladima@gmail.com>
Section: javascript
Priority: optional
Build-Depends: debhelper-compat (= 13)
 , dh-sequence-nodejs-no-lerna
 , dh-nodejs
 , cmake
 , node-fs-extra
 , node-clipanion
 , node-lodash-packages
 , node-typanion
 , node-enquirer
 , node-micromatch
 , node-ci-info
 , node-semver
 , node-react
 , node-diff
 , node-zkochan-cmd-shim
 , node-ssri
 , node-tar-stream
 , node-p-limit
 , node-camelcase
 , node-fast-glob
 , node-globby
 , node-cli-truncate
 , node-type-fest
 , node-cli-boxes
 , node-indent-string
 , node-widest-line
 , node-react-reconciler
 , node-string-width
 , node-cli-cursor
 , node-ansi-escapes
 , node-signal-exit
 , node-auto-bind
 , node-types-is-ci
 , node-is-upper-case
 , node-stack-utils
 , node-code-excerpt
 , node-ws
 , node-rollup-plugin-commonjs
 , node-rollup-plugin-node-resolve
 , node-lodash
 , node-is-fullwidth-code-point
 , node-ansi-styles
 , node-is-ci
 , node-slice-ansi
 , node-wrap-ansi
 , node-strip-ansi
 , node-got
 , node-tar
 , node-js-yaml
 , node-arg
 , ava <!nocheck>
 , ts-node
 , node-sinon <!nocheck>
 , node-boxen
 , jest <!nocheck>
 , mocha <!nocheck>
 , rollup
 , esbuild
 , node-rollup-pluginutils
 , node-typescript
 , emscripten
 , node-tap <!nocheck>
 , node-pirates
 , node-chalk
 , node-rollup-plugin-json
 , node-rollup-plugin-terser
 , node-tslib
 , node-babel7
 , node-rollup-plugin-babel
 , node-magic-string
 , node-estree-walker
 , node-acorn
 , node-rollup-plugin-typescript2
 , node-rollup-plugin-alias
 , node-csstype
 , node-prop-types
 , git
 , rsync
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/js-team/node-yarnpkg
Vcs-Git: https://salsa.debian.org/js-team/node-yarnpkg.git
Homepage: https://github.com/yarnpkg/berry
Rules-Requires-Root: no

Package: yarnpkg
Architecture: all
Depends: ${misc:Depends}
 , nodejs:any
 , git
 , node-tslib
 , node-clipanion
 , node-lodash
 , node-ci-info
 , node-enquirer
 , node-micromatch
 , node-semver
 , node-typanion
 , node-diff
 , node-react
 , node-zkochan-cmd-shim
 , node-ssri
 , node-tar-stream
 , node-p-limit
 , node-chalk
 , esbuild
 , node-camelcase
 , node-fast-glob
 , node-got
 , node-strip-ansi
 , node-tar
 , node-globby
 , node-typescript
 , node-js-yaml
 , node-ansi-styles
 , node-is-fullwidth-code-point
 , node-ansi-escapes
 , node-auto-bind
 , node-cli-boxes
 , node-cli-cursor
 , node-cli-truncate
 , node-code-excerpt
 , node-indent-string
 , node-is-ci
 , node-is-lower-case
 , node-is-upper-case
 , node-react-reconciler
 , node-scheduler
 , node-signal-exit
 , node-slice-ansi
 , node-stack-utils
 , node-string-width
 , node-type-fest
 , node-widest-line
 , node-wrap-ansi
 , node-ws
Provides: ${nodejs:Provides}
Description: Fast, reliable, and secure dependency management
 Yarn is a modern package manager split into various packages. Its novel
 architecture allows one to do things currently impossible with existing
 solutions:
  Yarn supports plugins; adding a plugin is as simple as adding it into your
  repository.
  Yarn supports Node by default but isn't limited to it - plugins can add
  support for other languages.
  Yarn supports workspaces natively, and its CLI takes advantage of that.
  Yarn uses a bash-like portable shell to make package scripts portable
  across of Windows, Linux, and macOS.
  Yarn is first and foremost a Node API that can be used programmatically
  (via @yarnpkg/core).
  Yarn is written in TypeScript and is fully type-checked.
 .
 This package provides Yarn Modern (berry). If you are looking for Yarn
 Classic (1.x), install node-corepack.
 .
 Node.js is an event-based server-side JavaScript engine.
